//log.h
#ifndef _LOG_H_
#define _LOG_H_

#include <string>
#include <ostream>
//#include <sstream>
#include <set>

#include "singleton.h"

#define _LOGFOLDER_ "/storage/sdcard0/code/guppy/log/"
//internalDataPath
#define _DEFAULTLOGFILE_ "log"

//#define DBGLOG *(Singleton<LOG>::Instance())
#define DBGLOG (*Log::Instance())["debug"]
#define LOG(LOGT) (*Log::Instance())[LOGT]

/*
class LOG: public cOBJECT{
	public:
		//void Write(std::string file, ...);
		//std::ostream&
				void operator << (std::string text);
};
*/

class Log: public cOBJECT, public Singleton<Log>{
	private:
		std::set<std::string> used;
		std::string LogName;
		
	public:
		Log();
		void operator << (std::string Text);
		Log& operator [] ( const std::string Logtype);
};



#endif