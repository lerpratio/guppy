#pragma once 

#include "memory.h"
#include "log.h"

//#include "memory.h"

namespace Guppy{
//----


//template which holds a stack of raw data of T type and of Buffersize length
template <class T, unsigned int BufferSize>
	class Buffer: public cOBJECT{
		private:
			T* Buff;
						
		public:
		//
			Buffer(){
				Buff = new T[BufferSize];
			};
		//
			~Buffer(){
				delete[] Buff;
			};
		//	
			T& operator[] (unsigned int Index){
			//GTFO if accessor wrong
				if (Index > (BufferSize-1)){
					DBGLOG << "Buffer Access Error!!\n";
					assert(	(Index >= 0)	&& (Index < BufferSize) );	
				}
			//return
				return Buff[Index];
			};
		
	};

//rawdata buffer// pointerable edition!
template <class T>
class DataBuffer: public cOBJECT{
	public:
		//===
		DataBuffer(){
			Data = 0;
			Initialized = false;
			DataSize = 0;
		};
		//===
		DataBuffer(unsigned long Size){
			Data = new T[Size];
			if (!Data){
				Initialized = false;
				DataSize = 0;
				return;
			}
			DataSize = Size;
			Initialized = true;
		}
		//===
		~DataBuffer(){
			if (Data){
				delete[] Data;
			}
		};
		//===
		//operators
		T& operator[](unsigned long Accessor){
			//if (	(Accessor >= DataSize)	||	(Accessor < 0)	){
				assert(	(Accessor < DataSize)	&&	(Accessor >= 0)	&& (Initialized)	);
			
			//}
			
			return Data[Accessor];
		};
		
		
		//return type pointer;
		operator T* () const{
			return Data;
		};
		//ready?
		bool IsInitialized(){
			return Initialized;
		};
		
	private:
		T* Data;
		unsigned long DataSize;
		bool Initialized;
};

//ringbuffer blah blah blah


//----
}