#include "entity.h"
#include "log.h"

//////
//Component
//////
Component::~Component(){};
//
Component::Component(Entity * Owner){
	Parent = Owner;
};
//
void Component::SetParent(Entity* Owner){
	Parent = Owner;
};
//
const Entity* Component::GetParent(){
	return Parent;
};

//////
//Entity
//////
		Entity::Entity(){
			Name = "Unnamed";
		};
		//
		Entity::Entity(ID Title){
			Name = Title;
		};
		//
		const ID& Entity::GetName(){
			return Name;
		};
		//
		void Entity::SetName(ID Title){
			Name = Title;
		};
	//component management
		bool Entity::AddComponent(Component* Comp){
			ComponentTable::iterator it = ComponentList.find(Comp->GetType() );
			//DBGLOG << "compad ";
			//DBGLOG << Comp->GetType();
			if (it == ComponentList.end()){
				ComponentList.insert(ComponentTable::value_type(Comp->GetType(), Comp ) );
			}else{
				//should i kill it?
				DBGLOG <<"no add c\n";
				//delete Comp;
				return false;
			}
			return 1;
		};
		//
		Component* Entity::GetComponent(ID type){
			ComponentTable::iterator it = ComponentList.find(type);
				if (it != ComponentList.end()){
					//DBGLOG << (*it).first;
					return (*it).second;
				} else {
					DBGLOG << "nofind \n";
					return 0;
				}
		};
		//
		void Entity::ClearComponents(){
			//iterate through each component and delete
			ComponentTable::iterator it;
			for (it = ComponentList.begin(); it != ComponentList.end(); it++){
				delete (*it).second;
			}
			
			ComponentList.clear();
		};
//////
//Entity Manager
//////
EntityManager::~EntityManager(){
	//on destruction kill its children
	ClearEntities();
}
//
Entity* EntityManager::AddEntity(Entity* Ent){
	Entities.push_back(Ent);
	return Ent;
}
//
void EntityManager::RemoveEntity(Entity* Ent){
	EntityList::iterator it;
	
	for(it = Entities.begin(); it != Entities.end(); it++){
		if(*it == Ent){
			delete *it;
			Entities.erase(it);
		}
	}
}
//
void EntityManager::ClearEntities(){
EntityList::iterator it;
	
	for(it = Entities.begin(); it != Entities.end(); it++){
		delete *it;
		Entities.erase(it);
	}
	
	Entities.clear();
}
//
//EntityManager::~EntityManager(){}//