//////
#include "engine.h"
#include "log.h"
#include "system.h"



namespace Guppy{
//////
Engine::Engine(){
	//initialize things
		Running = 0;
		Initialized = 0;
		mSystem = 0;
	//reset managers
}
//
Engine::~Engine(){
	//just in case!
		Terminate();
}
//
bool Engine::Initialize(){
	LOG("guppy") << "Initializing Guppy...\n";
	
	mSystem = SYS_ANDROID::Instance();//Singleton<SYS_ANDROID>::Instance();
	if (!mSystem){
		LOG("guppy") << "Msys failed\n";
	}
	//if(!	
	mSystem->Init();
	/*){
		LOG("guppy") << "System fail\n";
		Initialized = false;
		return false;
	}*/
	
	Initialized = true;
	LOG("guppy") << "Initialized!\n";
	return true;
};
//
void Engine::Terminate(){
	if (Initialized) {
		LOG("guppy") << "Terminating Guppy...\n";

		mSystem->Quit();

		
		Initialized = false;
		
		LOG("guppy") << "Guppy Terminated!\n";
	}else{
		LOG("guppy") << "Guppy already terminated.\n";
	}
}
//
bool Engine::Run(){
	Running = true;
	LOG("guppy") << "Running loop.\n";
		
	while (Running){
		//do things
		mSystem->HandleEvents();
		//Stop();
	}
}

void Engine::Stop(){
	Running = false;
}

//////
}


