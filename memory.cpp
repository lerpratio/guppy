#include "memory.h"
#include "entity.h"
#include "log.h"

//  using namespace std;
//
using std::list;
//using std::list::iterator;

////////////
//Object
////////////

list<cOBJECT*> cOBJECT::mLive;
list<cOBJECT*> cOBJECT::mDead;

cOBJECT::cOBJECT(){
	cOBJECT::mLive.push_back(this);
	mRef = 0;
}
///////
cOBJECT::~cOBJECT(){
}

///////
void cOBJECT::mAddRef(){
	mRef++;
}
//////
void cOBJECT::mDelRef(){
	mRef--;
	if (mRef  <= 0){
		mLive.remove(this);
		mDead.push_back(this);
	}
}
//////
int cOBJECT::mCheckRef(){
	return mRef;
}
//////
void cOBJECT::mClean(){
	if (mDead.empty()){
		//DBGLOG << "nothing there!\n";
		return;
	}
	DBGLOG << "clearing mem...";
	
	for (list<cOBJECT *> ::iterator iter = mDead.begin() ; iter != mDead.end(); iter++){
		delete (*iter);
	}
	
	mDead.clear();
	DBGLOG << "Dead memory cleared! \n";

}
//////
void cOBJECT::mClear(){
 	DBGLOG << "completely clearing memory. \n";
 	mClean();
 
	if (	mLive.empty()	){return;}
 
	for(list<cOBJECT *>::iterator iter = mLive.begin(); iter != mLive.end(); iter++){
		delete (*iter);
	}
	mLive.clear();
}
//////
 
////////////
//Smart Pointer
////////////
//template< class T>cPOINTER<T>::cPOINTER(){
//	this->pointer = 0;
//}
//////
//template< class T>cPOINTER<T>::~cPOINTER(){
	
	
//}
//////
//template <class T> cPOINTER<T>::cPOINTER(cPOINTER & pTmp){
//	this->pointer = pTmp.pointer;
//	pTmp->mAddRef();
//}
//////
/*
template <class T> inline cPOINTER<T>& cPOINTER<T>::operator = (cPOINTER& pTmp){ 
	this.pointer = pTmp.pointer;
	pTmp.mAddRef();
}
*/
//////
/*
template <class T> inline T& cPOINTER<T>::operator = (T* pTmp){ 
	this.pointer = pTmp;
	pTmp.mAddRef();
}
*/
//////
/*
template <class T> inline T& cPOINTER<T>:: operator *(){
	return *pointer;
}
//////
template <class T> inline T* cPOINTER<T>:: operator ->(){
	return pointer;
}
//////*/
//template <class T> cPOINTER<T>::
//////



/////////
//template class cPOINTER<Entity>;
//template class cPOINTER<Component>;
/////////