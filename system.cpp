//includes
	#include "memory.h"
	#include "system.h"
	#include "log.h"
	
	#include "singleton.h"
	#include "renderer.h"
	
namespace Guppy {
//////
//sys
//////

//////
bool SYS::IsInitialized(){
	return Initialized;
}
//////

//////
//sys_andy
//////

//SYS_ANDROID::~SYS_ANDROID(){	Quit();}

bool SYS_ANDROID::Init(){
	//null! android needs to be readied first
		if(!App){
    	Initialized = false;
    	LOG("system") << "System intialization error\n";
    	return false;
    }
   //initialize callbacks    
		App->userData = this;
		App->onAppCmd = ANDY_Handler;
		App->onInputEvent = ANDY_Input;
    app_dummy();
	
	//
		//HandleEvents();
	//renderer
		if (	GetAndroidWindow()	){
			Singleton<Renderer_GLES>::Instance()->Initialize();
    }
   //all done
    Initialized = true;
    LOG("system") << "System intialized\n";
    
    return true;
}
//////

bool SYS_ANDROID::Init(android_app * ppA){
	//set app pointer 
    App = ppA;
  //check validity
		return Init();
}
//////
void SYS_ANDROID::Quit(){
	Singleton<Renderer_GLES>::Instance()->Terminate();
	Initialized = false;
	DBGLOG << "Terminated!\n";
	
	return;
}

//////
void SYS_ANDROID::HandleEvents(){
	int ident; // stores return value
	int event; // event count?
	android_poll_source * source; // event data
	//DBGLOG << "Handled\n";	
	
	//alooper desc
	// <file descriptor has data or error code> ALooper_pollAll (timeout millisecs, FD, event, pointer to data pointer);
	//in short, if there's data to be processed, loop until it's processed. 
	while(	(	ident = ALooper_pollAll(0 ,NULL, &event,(void **)&source)	) >=0	){
		
		//process event
		if (source != NULL){
			source->process(App,source);
		}
		
		//process sensor
		
		//process exit;
		if (App->destroyRequested != 0){
			Quit();
			return;
		}
	}
//event check over
		
}
//////
void SYS_ANDROID::Update(){
	//update sys
}
//////
/*static*/ void SYS_ANDROID::ANDY_Handler(android_app* ppa, int32_t cmd){
	//intitialize singleton for use in static function
	cPOINTER<SYS_ANDROID> I;
	I = Singleton<SYS_ANDROID>::Instance();

	switch (cmd){
		case APP_CMD_SAVE_STATE:
			//set save data to state data
			//	I->App->savedState = new int(Savestate);
		break;
		case APP_CMD_INIT_WINDOW:
			//initialize if it hasnt been already
				if (I->App->window != 0){
					//there is a window, pop it open
						I->Init();
				} 
		break;
		case APP_CMD_TERM_WINDOW:
			//kill the window
				I->Quit();
		break;
		case APP_CMD_GAINED_FOCUS:
			//enable sensors
			//resume engine
		break;
		case APP_CMD_LOST_FOCUS:
			// disable sensors
			// pause engine
		break;
	}
return;
};
//////
/*static*/ int32_t SYS_ANDROID::ANDY_Input(android_app* ppa, AInputEvent* event){
	return 0;
};
//////

//////
android_app* SYS_ANDROID::GetAndroidApp(){
	//returns android_app
	return App;
}
//////

//////
ANativeWindow* SYS_ANDROID::GetAndroidWindow(){
	//returns Native window
	return App->window;
}
//////

bool SYS_ANDROID::SetAndroidApp(android_app * ppA){
	App = ppA;
	return (App);
}

//
}

