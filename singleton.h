#ifndef _SINGLETON_H_
#define _SINGLETON_H_
//header
#include "memory.h"

//
//#define NDEBUG
#include <cassert>

template <class T>
class Singleton{
	public:
		static T* Instance(){
			static T single;
			
			if(ptr){
				return ptr;
			} else{
				ptr = &single;
				assert(ptr);
				return ptr;
			}
		};
		//
		~Singleton(){
			if(ptr) {
				ptr = 0;
			}
		}
		//
		//
	private:
		//
		//Standard pointer
		//static T* ptr;
		//Cpointer pointer
		static cPOINTER<T> ptr;
		
		//this variant of singleton protects this innately 
		//Singleton(){};
		//Singleton(Singleton&){};
		
		
};

//what's this for again i forget. // defines ptr for all variants of cpointer
template<class T> cPOINTER<T> Singleton<T>::ptr;

#endif 