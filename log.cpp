//log.cpp
#include "log.h"

#include <fstream>
#include <sstream>

using std::string;
using std::ofstream;
using std::ostringstream;
using std::set;
//////
Log::Log(){
	//nothing
}
//

void Log::operator << (string Text){
	if (LogName.empty() ){
		LogName = _DEFAULTLOGFILE_ ;
	}
	//build path
		ostringstream logpath;
		logpath << _LOGFOLDER_ << LogName << ".txt";
		
	
	ofstream F;
	if(used.find(LogName) != used.end() ) {
		//open file and append
			F.open(logpath.str().c_str(), std::ios::app);
	} else {
		//insert name then erase contents
			F.open(logpath.str().c_str(), std::ios::in | std::ios::trunc);
			used.insert(LogName);
	}
		//check if opened
			if(!F.is_open()){ 
				//problem!
				//cout <<"log file access error.\n";
				assert(F.is_open());
			}
		
	//write
		F << Text;
		//cout << text;
	//close
		F.close();

}

//////
Log& Log::operator [] (string Logtype){
		LogName = Logtype;
	return *this;
}
//////




