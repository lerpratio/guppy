/*
//event handling mumbo jumbo
*/

#ifndef _EVENT_H_
#define _EVENT_H_

#include <string>
#include <map>
#include <queue>

#include "memory.h"

//pre-declares
//class EVENT;
//class EVENTLISTENER;
//class EVENTMANAGER;


//Event -- contains tokenized string which describes event
class EVENT{
	private:
		//std::string mEvent;		//stored message
		std::vector<std::string> mTokens;		//tokenized string
		int mCount; // token count; 
	public:
		//con/de-structors
			EVENT(); // 
			EVENT(std::string pEvent);
			~EVENT();
		//accessors
			//std::string GetEvent();
			//std::string GetType();	
			std::string& operator[](unsigned int pAccess);
			EVENT& operator=(std::string pMessage);
			
		//internal funcs
			bool Process(std::string pInput);		//process input string
};

//Reciever -- virtual class that lets derivers recieve events and processes them
class EVENTHANDLER: virtual public cOBJECT{
	public:
		virtual void HandleEvents(EVENT) = 0;
};

//Dispatcher-- recieves events and dispatches them
class EVENTMANAGER: virtual public cOBJECT{
	public:
	//external typedefs
		
	//suscribe listener to 'pType' magazine
		bool Register(	std::string EventType, EVENTHANDLER * Callback	);
	//unsuscribe listener to 'pType' magazine
		void Unregister(	std::string EventType, EVENTHANDLER * Callback	);
	//send message to all suscribers
		void Dispatch();
	//push event into queue
		void PushEvent(EVENT Pop);
		void PushEvent(std::string);
		
	private:
	//internal typedefs
		typedef std::multimap<std::string, cPOINTER<EVENTHANDLER> > SubscriberList;
		typedef SubscriberList::iterator SubscriberListIterator;
		typedef std::pair<std::string, cPOINTER<EVENTHANDLER> > SubscriberListPair;
		typedef std::pair<	SubscriberListIterator, SubscriberListIterator > SubscriberListRange;
		
		typedef std::queue<EVENT> EventQueue;
	//subscription database
		SubscriberList Subscribers;
	//Event buffer
		EventQueue Events;
};

#endif