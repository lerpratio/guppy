#pragma once 

#include <map>
#include <string>

#include "memory.h"
#include "singleton.h"
#include "buffer.h"

namespace Guppy{
/////

//////
//resource manager
//////

//======
//load resources in engine compatible format
//not really managing memory 100% manually yet
class Resource: public cOBJECT{	
	public:
		Resource();
		virtual ~Resource() = 0;
		Resource(std::string FilePath);
		
		//void AddReference();
		//void RemoveReference();
		
		unsigned long GetSize();

		bool IsInitialized();
	private:
		//int ReferenceCount;
		
	protected:
		unsigned long FileSize;
		bool Initialized;
		std::string Path;
};

//======
enum ResourceType{
	RESOURCE_BINARY = 0,
	RESOURCE_TEXT
};
//======
class ResourceManager: public cOBJECT{
	public:
		typedef std::map< std::string, Resource * > ResourceTable;
		
		 
		 bool LoadResource(std::string Path, ResourceType Tipe);
		 bool CheckResource(std::string Path);
		 void RemoveResource(std::string Path);
		 void ClearResources();
	private:
		ResourceTable Resources;
		 				
};

//======
class ResourceBinary: public Resource{
	public:
		ResourceBinary();
		~ResourceBinary();
		ResourceBinary(std::string Filepath);
		
	private:
		cPOINTER < DataBuffer<char> > Data;
};
//======
		
////
}