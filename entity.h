#ifndef _ENTITY_H_
#define _ENTITY_H_

//includes
#include <string>
#include <map>
#include <vector>

#include "memory.h"
//#include "util.h"

class Entity;

typedef std::string ID;

//Component
class Component: public cOBJECT{
	private:
		Entity * Parent;		
		
	public:
	//
		Component(): Parent(0){};
		Component(Entity* Owner);
		virtual ~Component() = 0;
	//
		virtual const ID GetType() = 0;
		virtual const ID GetName() = 0;
	//
		virtual void Update() = 0;
	//
		void SetParent(Entity* owner);
		const Entity* GetParent();
	//
		MEMSIZE(*this)
};

//
class Entity: virtual public cOBJECT{
	private:
		typedef std::map< ID , Component* > ComponentTable;
		ComponentTable ComponentList;	
		ID Name;
	public:
	//constructor
		Entity();
		Entity(ID Title);
	//name interface
		const ID& GetName();
		void SetName(ID Title);
	//component management
		bool AddComponent(Component * Comp);
		Component* GetComponent(ID type);
		void ClearComponents();
	//
		MEMSIZE(this)
};

class EntityManager: virtual public cOBJECT{
	private:
		typedef std::vector<Entity*> EntityList;
		EntityList Entities;
	public:
	//EntityManager()
	~EntityManager();
	
	Entity* AddEntity(Entity* Ent);
	void RemoveEntity(Entity * Ent);
	//RemoveEntity(ID text);
	void ClearEntities();
};

#endif