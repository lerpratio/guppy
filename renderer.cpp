#include "renderer.h"
//

//
#include "singleton.h"
#include "system.h"
#include "log.h"
#include "engine.h"
#include "system.h"

#include <string>

namespace Guppy{

//////
//Quad2D
///////
RenderableType Quad2D::GetType(){
	return GUPPY_QUAD2D;
}

void Quad2D:: Draw(){
	vec2D Quad[3];
 
}; 


//////
//Renderer
//////
Renderer::Renderer(){
	//nothin
}

Renderer::~Renderer(){
	//call terminate just in case
//		Terminate();
}

void Renderer::Terminate(){
	//dummy function
} 
//////
//Renderer_GLES
//////

Renderer_GLES::~Renderer_GLES(){
		Terminate();
}

bool Renderer_GLES::Initialize(){
	//acquire app info and set up dislpay
		//get display
	Screen = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	
		
	//intialize
	if (	!eglInitialize(Screen,0,0)	){
		LOG("opengles") << "Renderer failed to initialize";
		return false;
	};
	
		//LOG("opengles") << "Renderer initialized \n"
	//rebug
			std::string debuginfo;
			//LOG("opengles") << eglQueryString(Screen, EGL_CLIENT_APIS) << "\n"; 
			debuginfo = "EGL info\nClient Apis: ";
			debuginfo += eglQueryString(Screen, EGL_CLIENT_APIS);
			debuginfo += "\n";
			
			debuginfo += "Egl Extensions: ";
			debuginfo += eglQueryString(Screen, EGL_EXTENSIONS);
			debuginfo += "\n";
			
			debuginfo += "Vendor info: ";
			debuginfo += eglQueryString(Screen, EGL_VENDOR);
			debuginfo += "\n";
			
			debuginfo += "Version: ";
			debuginfo += eglQueryString(Screen, EGL_VERSION);
			debuginfo += "\n";
			
			
			LOG("opengles") << debuginfo;
			
			
			
	//configure configuration
		//array of ints. <attribute, value>. terminated with EGL_NONE
	EGLint attrib[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_BLUE_SIZE, 8,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_NONE
	};
	
	//select '1' screen type that matches config
	EGLConfig Cfg;
	EGLint Numcfg;
	
	eglChooseConfig(Screen,attrib,&Cfg,1, &Numcfg);
	// get info for napp
	EGLint format; 
	eglGetConfigAttrib(Screen, Cfg, EGL_NATIVE_VISUAL_ID, &format );
	
	//send to napp
		ANativeWindow* wind =	SYS_ANDROID::Instance()->GetAndroidWindow();
		if (!wind){
			LOG("opengles") << "Window not available!\n";
			return 1;
		}
		
			ANativeWindow_setBuffersGeometry(wind,0,0,format);
		
	//create surface
		Surface = eglCreateWindowSurface(Screen, Cfg, wind, NULL);
	//create context
		Context = eglCreateContext(Screen, Cfg, NULL, NULL) ;
		
	//enable surface
	if(	eglMakeCurrent(Screen,Surface,Surface, Context) == EGL_FALSE){
		// error enabling surface
		LOG("opengles") << "Error enabling surface\n";
		
		return 1;
	};
	
	//get dimensions
		//eglQuerySurface(Screen,Surface, EGL_WIDTH, &Native.Width);
		//eglQuerySurface(Screen,Surface, EGL_HEIGHT, &Native.Height);
		
	//////initialize renderer
		glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_FASTEST);
		glEnable(GL_CULL_FACE);
		glShadeModel(GL_SMOOTH);
		glDisable(GL_DEPTH_TEST);
	//////
	
		glClearColor(0.5,0.5,0.5,1);
		glClear(GL_COLOR_BUFFER_BIT);
		eglSwapBuffers(Screen, Surface);
}
///


void Renderer_GLES::Terminate(){
	//teardown dislpay
	//cleanup;
    if (    Screen != EGL_NO_DISPLAY){
        //disable the screen
            eglMakeCurrent(Screen, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        //kill surface and context
            if(    Surface != EGL_NO_SURFACE){
                eglDestroySurface(Screen, Surface);
            } 
            if(    Context != EGL_NO_CONTEXT){
                eglDestroyContext(Screen, Context);
            } 
    }
    //all clean? then quit
        eglTerminate(Screen);
    //Reset pointers;
    Screen = EGL_NO_DISPLAY;
    Surface = EGL_NO_SURFACE;
    Context = EGL_NO_CONTEXT;
    
    return;
	
}

void Renderer_GLES::Draw(){
	//process renderable stack	
		glClearColor(0.5,0.5,0.5,1);
		glClear(GL_COLOR_BUFFER_BIT);
		eglSwapBuffers(Screen, Surface);

}

void Renderer_GLES::Clear(){
	//clear the screen 
}
void Renderer_GLES::UpdateScreen(){
	// any extra processes?
}

}