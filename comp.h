#include "entity.h"




class Vis_2D;
class C_Position;
//////////
class Vis_2D:public Component{
	private:
		
	public:
		Vis_2D(): Width(1), Height(1){} ;
		~Vis_2D(){};

		const ID GetType(){return ID("Visual");};
		const ID GetName(){return ID("2D_image");};
		void Update(){};
		
		
		void Draw(){};
		float Width;
		float Height;
};
///////////

class C_Position: public Component{
		private:
		public:
			C_Position(): X(0),Y(0), Z(0){};
			~C_Position(){};
		
		const ID GetType(){return ID("Position");};
		const ID GetName(){return ID("Location");};
		void Update(){};
		
		float X,Y,Z;
		
};