#include "entity.h"
#include "comp.h"

class eSprite: public Entity, virtual public cOBJECT{
private:
public:
	eSprite(ID Title){
		this->AddComponent( new Vis_2D );
		this->AddComponent( new C_Position );
		SetName(Title);
	};
	//eSprite()
	~eSprite();
};