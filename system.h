#ifndef _SYSTEM_H_
#define _SYSTEM_H_
//
	#include <android_native_app_glue.h>

//
	#include "memory.h"
	#include "singleton.h"

namespace Guppy{
struct DISPLAY{
	int Width;
	int Height;
};

class SYS: public cOBJECT{
	private:

	protected:
	//Initialized?
		bool Initialized;
	//Screen
		DISPLAY Native;
	public:
		virtual bool Init() =0;
		virtual void HandleEvents() =0;
		virtual void Update() =0;
		virtual void Quit() =0;
			//TODO:move this
		bool IsInitialized();
};
//////
class SYS_ANDROID: public SYS, public Singleton<SYS_ANDROID> {
	private:
	//android app
		android_app * App;

	public:
//
		SYS_ANDROID(): App(0){};
//		~SYS_ANDROID();
		
	//initialization stuff
		bool Init();
		void Quit();
	//android entry point
	//sets app*, call to initialize
		bool Init(android_app *);
	//
		void HandleEvents();
		void Update();
	//TODO: SaveState data
		int Savestate;
	//static callback functions!
		static void     ANDY_Handler(android_app* ppa, int32_t cmd);
		static int32_t  ANDY_Input(android_app* ppa, AInputEvent* event);
	//Return System state data
		android_app* GetAndroidApp();
		ANativeWindow* GetAndroidWindow();
		bool SetAndroidApp(android_app * ppA);
};




//
}
#endif