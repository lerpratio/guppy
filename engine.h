#ifndef _ENGINE_H_
#define _ENGINE_H_

//include non pointers
#include "memory.h"
#include "singleton.h"
#include "system.h"
#include "event.h"

namespace Guppy{

class Engine: virtual public cOBJECT, public Singleton<Engine>, public EVENTMANAGER{
	private:
		//Engine States
			bool Running;
			bool Initialized;
	public:			
		//sub-system pointers
			cPOINTER<SYS> mSystem;
			//cPOINTER<Renderer> * sistema;
 
		//constructors
		Engine();
		~Engine();
		//
		bool Initialize();
		void Terminate();
		bool Run();
		void Stop();
		
};
//
}
#endif