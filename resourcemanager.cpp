
#include "resourcemanager.h"
#include <fstream>

using std::ifstream;


namespace Guppy{
/////

//////
//resource
//////
Resource::Resource(){
	Initialized = false;
};
//
Resource::~Resource(){
	//nothing
};
//
Resource::Resource(std::string FilePath){
	Path = FilePath;
	//allocate buffer
	//parse data
	//Initialized = true;
};
//

unsigned long Resource::GetSize(){
	return FileSize;
};
//

bool Resource::IsInitialized(){
	return Initialized;
};
//

//Resource::Resource(){};



//Resource::Resource(){};



//////
//resource binary
//////
ResourceBinary::ResourceBinary(){
	Initialized = false;
	FileSize = 0;
};
//
ResourceBinary::~ResourceBinary(){};
//
ResourceBinary::ResourceBinary(std::string FilePath){
	Path = FilePath;
	//open file
		ifstream File(	FilePath.c_str(), ifstream::in | ifstream::binary | ifstream::ate);
		if (!File.is_open() ){
			Initialized = false;
			FileSize = 0;
			return;
		}
	//get filesize
		//File.seekg(0,ios::end);
		FileSize = File.tellg();
		File.seekg(0,ifstream::beg);
		
	//allocate buffer
		Data = new DataBuffer<char>(FileSize);
		if(!Data){
			Initialized = false;
			FileSize = 0;
			return;
		}
	//load data
		File.read(*Data, FileSize);
	//close file
		File.close();
	//done
		Initialized = true;
};

//////
//resource manager
//////
bool ResourceManager::LoadResource(std::string Path, ResourceType RType){
	switch (RType){
		case RESOURCE_TEXT:
			
		break;
		
		case RESOURCE_BINARY:
			if (Resources[Path]){
				return false;
			}
			Resources.insert(ResourceTable::value_type(Path, new ResourceBinary(Path) )	);
		break;
		
		default:
		break;
	}
	
	//Resources[Path]  
	return true;
};
//
bool ResourceManager::CheckResource(std::string Key){

};
//
void ResourceManager::RemoveResource(std::string Key){

};
//
void ResourceManager::ClearResources(){

};
//
//
}