#ifndef _MEMORY_H_
#define _MEMORY_H_
//includes
	#include <list>
	#include <cassert>
//macros
	#define MEMSIZE(X) unsigned int Size(){return sizeof(X);}
//
class cOBJECT{
	private:
		int mRef;
		static std::list<cOBJECT*> mLive;
		static std::list<cOBJECT*> mDead;
		
 	public:
 		//constructors
 			cOBJECT();
 			//cOBJECT(cOBJECT& pOBJ);
 			//
 		//operators
 			//cOBJECT& operator = (cOBJECT& pOBJ);
 		//destructor
 			~cOBJECT();
 		//
 		
 		virtual unsigned int Size()		{return sizeof(cOBJECT);};
 		static int mTotalMem();
 		
 		void mAddRef();
 		void mDelRef();
 		int mCheckRef();
 		
 		static void mClean();
 		static void mClear();
 		
 		
};

template <class T>
class cPOINTER{
	protected:
		T* pointer;
	public:
		cPOINTER(){
			pointer = 0;
		};
		//
		~cPOINTER(){
			pointer->mDelRef();
		};
		//
		cPOINTER(cPOINTER& pTmp){
			this->pointer = pTmp.pointer;
			pTmp->mAddRef();
		};

		//
		cPOINTER(T* tmpptr){
			pointer = tmpptr;
			//assert
		};
	// need this next one for some raisin;
		cPOINTER(const cPOINTER& pTMP){
			this->pointer = pTMP.pointer;
			this->pointer->mAddRef();
		}
		
	//operators
		inline cPOINTER& operator =(cPOINTER& pTmp) {
		};
		//
		inline T& operator =(T* pTmp){
			//if already assigned, delete reference
				if (pointer){
					pointer->mDelRef();	
				}
			//assign
				pointer = pTmp;
			//if its a pointer then add a reference	
				if(pointer){
					pointer->mAddRef();
				}
				return *pointer;
		};
		//
		inline T& operator *(){
			assert(pointer);
			return *pointer;
		}; 
		//
		inline T* operator->(){
			assert(pointer);
			return pointer;
		};
	//	inline T* operator ==(cPOINTER& pTmp);
		inline operator T* () const{
			//assert(pointer);
			return pointer;
		}
};
#endif
