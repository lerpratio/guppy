#ifndef _INPUT_H_
#define _INPUT_H_

#include <map>

#include "event.h"
#include "singleton.h"
namespace Guppy{

class Input: virtual public cOBJECT, public Singleton<Input>{
	private:
			typedef std::map<std::string, float> InputTable;
			InputTable InputList;
	
	public:
	
		//virtual bool Initialize() = 0;
		//virtual void Terminate() = 0;
		
		void InputEvent(std::string Name, float state);
		//RangeEvent(name, state, min, max);
};

//
}
//
#endif
