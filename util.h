#ifndef _UTIL_H_
#define _UTIL_H_

namespace Guppy{
//////
struct vec2D{
	float x;
	float y;
};

struct vec3D{
	float x;
	float y;
	float z;
};

struct vec4D{
	float x;
	float y;
	float z;
	float w;
};

struct matrix4D{
	vec4D vector[3];
};

struct matrix3D{
	vec4D vector[2];
};

vec3D& operator+(vec3D A, vec3D B){
	vec3D result;
	result.x = A.x + B.x;
	result.y = A.y + B.y;
	result.z = A.z + B.z;
}

//////
}
#endif