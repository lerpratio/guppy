#include "input.h"

#include <string>
#include <sstream>

#include "engine.h"

using std::string;
using std::ostringstream;

/////////////
//input
/////////////
namespace Guppy{
//Input::Input(){}
//Input::~Input(){}

//
void Input::InputEvent(string Name, float State){
	InputList[Name] = State;
	
	ostringstream tmp;
	tmp << "Input " << Name << " " << State;
	
	Engine::Instance()->PushEvent(	tmp.str()	);
	
}
//

}
