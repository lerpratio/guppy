#include <cassert>
#include "event.h"
#include "log.h"
//

using std::string;
using std::pair;

//////
//Event
//////

EVENT::EVENT(){
	//Process("");
};
//
EVENT::EVENT(string pEvent){
	Process(pEvent);
};
//
EVENT::~EVENT(){
//	Process("");
};
//
string& EVENT::operator [](unsigned int pAccess){
	assert(pAccess < mTokens.size() ); // check if index is within limits 

	return mTokens[pAccess];	

}
//
EVENT& EVENT::operator =(string pMessage){
	Process(pMessage);

	return *this;
}
//
bool EVENT::Process(string pInput){
	unsigned int pos = 0;
	unsigned int offset = 0;
	string::iterator iter;
	string temp = "";
	bool done = false;
	mCount = 0;
	
	if(pInput.empty()){
		return false;
	}
	
	
	while (!done){
		pos = pInput.find(' ',offset);
			if (pos == string::npos){
				temp = pInput.substr(offset,temp.size()	);
				mTokens.push_back(temp);
				mCount++;
				done = true;
			}else{
				temp = pInput.substr(offset,pos-1);
				mTokens.push_back(temp);
				mCount++;
				offset = pos +1;
			}
	}
	return true;
}
//


//////
//EventHandler
//////


//////
//Eventmanager
//////
bool EVENTMANAGER::Register(string EventType, EVENTHANDLER * Callback){
	if(EventType.empty() || !Callback ){
		//maybe assert here? 
		return false;
	}
	Subscribers.insert(	SubscriberListPair(EventType, Callback)	);
	return true;
};
//

void EVENTMANAGER::Unregister(std::string EventType, EVENTHANDLER * Callback){
	SubscriberListRange range = Subscribers.equal_range(EventType);
	 
	if(range.first->first != EventType ){
		return;
	};
	
	for( SubscriberListIterator iter = range.first; iter != range.second; iter++){
		if (iter->second == Callback){
			Subscribers.erase(iter);
			//break;
		} 
	}
		
};
//

//Pump queue
void EVENTMANAGER::Dispatch(){
while (!Events.empty()){
//
	SubscriberListRange range = Subscribers.equal_range(	Events.front()[0]	);
	if (range.first == Subscribers.end()){
		//LOG("") << 
		//no listeners
		return;
	}
	
	for( SubscriberListIterator iter = range.first; iter != range.second; iter++	){
		iter->second->HandleEvents(Events.front());
	}
	
	Events.pop();
	//
	}
};
//
//add to queue
void EVENTMANAGER::PushEvent(EVENT Push){
	Events.push(Push);
	DBGLOG << Push[0];
};
//

//
void EVENTMANAGER::PushEvent(string Push){
	PushEvent(	EVENT(Push)	);
};
//
