#define DEBUG

//includes
#include <jni.h>
#include <errno.h>
#include <android_native_app_glue.h>

//
#include <sstream>

#include "log.h"
#include "engine.h"
#include "memory.h"

using Guppy::Engine;
using Guppy::SYS_ANDROID;

#include "buffer.h"

//standard main
//int main(int argc, char* arg[]){
//

//android main
void android_main(android_app * state){
	LOG("log") << "sup?\n";
	
	cPOINTER< Guppy::Buffer<unsigned char, 3 > > buffy;
	buffy = new Guppy::Buffer<unsigned char, 3 >;
	(*buffy)[0] = 'B';
	(*buffy)[1] = 'A';
	(*buffy)[2] = 'D';
	std::ostringstream butt;
	butt << (*buffy)[0] << (*buffy)[1]<< (*buffy)[2]<< '\n';
	DBGLOG << butt.str();
	delete buffy;
	
	
	cPOINTER<Engine> Gunpey = Engine::Instance();
	
	//randy
	//android setup
		SYS_ANDROID::Instance()->SetAndroidApp(state);
		//TODO: find way to get app state without hucking pointers everywhichwayandwhere
	//\randy
	
	Gunpey->Initialize();
	Gunpey->Run();
	Gunpey->Terminate();
	return;
}

