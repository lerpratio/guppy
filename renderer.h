//renderer
#ifndef _RENDERER_H_
#define _RENDERER_H_


#include <EGL/egl.h>
#include <GLES/gl.h>

#include <vector>

#include "memory.h"
#include "util.h"

namespace Guppy{
///////

enum MeshType{
	QUAD2D,
	MESH3D
	
};

//Mesh interface
class Mesh{
	MeshType Type;
};
//

//Mesh2D
class Mesh2D: public Mesh{
	vec2D point[3];
	
};

//renderable types
enum RenderableType{
	GUPPY_TYPEERR,
	GUPPY_QUAD2D,
	GUPPY_MODEL3D
};

//renderable interface
class Renderable: public cOBJECT{
	private:
		//vex buffer
		//elem buffer
		//tex buffers
		//shader program
		RenderableType Type;
		int Enabled;
	public:
		Renderable();
		~Renderable();
	//draw renderable to screen
		virtual void Draw() = 0;
	//Get Rendering type
		virtual RenderableType GetType() = 0;
	//Set/Unset Enabled status
		
};

class Quad2D: public Renderable{
	private:
		//vec2D points[3];
	public:
		RenderableType GetType();
		float x,y,w,h;
		void Draw();
};




class Renderer: public cOBJECT{
	private:
	//renderable table
		typedef std::vector<	cPOINTER<Renderable> > RenderList; 
	protected:
	public:
	//(de)Constructors
		Renderer();
		~Renderer();
	//Initialize screen
		virtual bool Initialize() = 0;
	//Terminate and clean up
		virtual void Terminate() = 0;

	//register objects/shader
		//bullshit here
		//register renderable
		//unregister renderable
		
	
	//Draw to screen;
		virtual void Draw() = 0;
	//Clear screen
		virtual void Clear() = 0;
	//Update/flip the screen // NOTE: Needed?
		virtual void UpdateScreen() = 0;
	//Initialized?
		bool Ready;
		bool IsInitialized();
		void SetInitialized();
	//Paused?
		bool Paused;
		bool IsPaused();
	//togglers
		void Pause();
		void Resume();
	
};

class Renderer_GLES: public Renderer{
	private:
	//renderer details
		EGLDisplay Screen;
		EGLContext Context;
		EGLSurface Surface;

	public:
	//Renderer constructors
		//Renderer_GLES();
		~Renderer_GLES();
	//virtual methods
		bool Initialize();
		void Terminate();
		void Draw();
		void Clear();
		void UpdateScreen();
};

}
#endif